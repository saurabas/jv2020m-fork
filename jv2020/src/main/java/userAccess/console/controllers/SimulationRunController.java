package userAccess.console.controllers;

import entitys.Simulation;
import userAccess.console.views.SimulationRunView;
import java.util.Scanner;

public class SimulationRunController {
	Scanner teclado = new Scanner(System.in);
	private int cycleSpeed = 0;
	
	private SimulationRunView simulationRunView;
	private Simulation simulation;
	
	public SimulationRunController(Simulation simulation) {	
		this.initSimulationRunControler(simulation);
		this.runSimulation();
		this.simulationRunView.showMessageOK();
	}
	
	private void initSimulationRunControler(Simulation simulation) {	
		this.simulationRunView = new SimulationRunView();
		this.simulation = simulation;
	}
	
	private void runSimulation() {
		int generation = 0; 
		int genCycles = this.simulation.getSimulationCycles();
		System.out.println("Establece el retardo de la ejecución en milisegundos");
		cycleSpeed = teclado.nextInt();
		do {			
			slowPushGrid();
			System.out.println("\nGeneración: " + generation);
			showGrid();			
			this.simulation.getWorld().updateGrid();
			generation++;
		}
		while (generation < genCycles);		
	}
	
	public SimulationRunView getSimulationRunView() {
		return this.simulationRunView;
	}

	public Simulation getSimulation() {	
		return this.simulation;
	}
	
	public void showGrid() {
		byte[][] worldGrid = this.simulation.getWorld().getGrid();

		for (int i = 0; i < worldGrid.length; i++) {
			for (int j = 0; j < worldGrid.length; j++) {		
				System.out.print((worldGrid[i][j] == 1) ? "|o" : "| ");
			}
			System.out.println("|");
		}
	}
	
	private void slowPushGrid() {
		try {
			Thread.sleep(cycleSpeed);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		for (int i = 0; i < 20; i++) {
			System.out.println("\n");
		}
	}
	
}
